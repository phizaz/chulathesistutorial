# README #

### ChulaThesisTutorial ###

This is created for undegraduate students in the Department of Mathematics and Computer science.
The template is compatible to the university thesis templates in 2012.
The material is used for LaTeX tutorial class in 2014 and 2015.

### Summary ###
* The main file is MathCSTutorial.tex
* All sources are compiled using XeTeX and BibTeX
* Exercises are Appendix A, B and files in Exercises
* Tested with MikTeX 2.9 on Windows and TeXLive on OSX 10.10 and TeXLive on Ubuntu 14.04